"use strict";

const fs = require("fs");
const zlib = require("zlib");
const glob = require("glob");
const path = require("path");

try {
  const publicPath = path.join(__dirname, "../public");

  const gzippable = glob.sync(
    `${publicPath}/**/?(*.js|*.css|*.svg|*.json|*.html)`
  );

  gzippable.forEach(file => {
    const content = fs.readFileSync(file);
    const zipped = zlib.gzipSync(content);
    fs.writeFileSync(`${file}.gz`, zipped);
  });

  console.log("✅  Success GZIP files");

  process.exit(0);
} catch (error) {
  console.log("⚠️  Failed GZIP files");
  console.log(error);
  process.exit(1);
}
