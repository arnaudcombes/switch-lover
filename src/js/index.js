let interval;

const input = document.getElementById("input");

// Mapping button index to each button
// Each joycon contains 16 buttons indexed
const MAP_KEYS = {
  0: "A",
  1: "X",
  2: "B",
  3: "Y",
  4: "RSL",
  5: "RSR",
  7: "2R",
  8: "R",
  9: "PLUS",
  10: "PRESSED ON JOYSTICK",
  11: "RA",
  12: "HOME",
  14: "R",
  15: "RT",
  16: "LEFT",
  17: "DOWN",
  18: "UP",
  19: "RIGHT",
  20: "LSL",
  21: "LSR",
  24: "MINUS",
  26: "LA",
  29: "CAPTURE",
  30: "L",
  31: "LT"
};

/**
 * Switch in landscape Mode
 * X Axis: -1 FULL LEFT / 1 FULL RIGHT
 * Y Axis: -1 FULL UP  / 1 FULL DOWN
 */

if (!("ongamepadconnected" in window)) {
  // No gamepad events available, poll instead.
  interval = setInterval(pollGamepads, 100);
}

function pollGamepads() {
  let gamepads = navigator.getGamepads ? navigator.getGamepads() : [];

  let gamepad = Object.values(gamepads)
    .filter(el => el)
    .find(g => g.id.indexOf("Joy-Con (R)") > -1);

  if (gamepad) {
    getJoyStickAxis(gamepad.axes);
    // Loop through all gamepad buttons pick out pressed ones
    for (let i = 0; i < gamepad.buttons.length; i++) {
      const currentButton = gamepad.buttons[i];
      if (currentButton.pressed) {
        const buttonPressed = MAP_KEYS[i];
        input.innerHTML = buttonPressed;
      }
    }
  }
}

function getJoyStickAxis(axis) {
  const [X, Y] = axis;

  const getWording = val => `JOYSTICK ${JSON.stringify(val)}`;

  const timeout = setTimeout(() => {
    input.innerHTML = getWording("0 / 0");
  }, 250);

  if (X === 0) {
    clearTimeout(timeout);
    return;
  }

  if (Y === 0) {
    clearTimeout(timeout);
    return;
  }

  const SCALAR = 1000;

  const computed = {
    x: Math.round(X * SCALAR) / SCALAR,
    y: Math.round(Y * SCALAR) / SCALAR
  };

  input.innerHTML = getWording(computed);
}
